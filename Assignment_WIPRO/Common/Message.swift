//
//  Message.swift
//  Assignment_WIPRO
//
//  Created by Chiranjeevi Challa on 31/05/19.
//  Copyright © 2019 Chiranjeevi Challa. All rights reserved.
//

import UIKit
import SCLAlertView

class Message: NSObject {
    
    static let shared:Message = {
        let shared = Message()
        return shared
    }()
    
    var alertPopup:SCLAlertView!
    
    var appearanceValue:SCLAlertView.SCLAppearance!
    
    private override init()
    {
        appearanceValue = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "Helvetica", size: 18)!,
            kTextFont: UIFont(name: "Helvetica", size: 14)!,
            showCloseButton: false,
            shouldAutoDismiss: false
        )

    }
    
    func show(_ title:String , message:String ,firstBtnTitle:String , secondBtnTitle:String , style:SCLAlertViewStyle, timeout:TimeInterval = 2.0, completed: @escaping (Bool) -> ())
    {
        
        self.hide()
        alertPopup = SCLAlertView(appearance: appearanceValue)
        
        print("alertPopup",alertPopup)
        if firstBtnTitle != ""
        {
            _ =  alertPopup.addButton("ok") {
                completed(true)
            }
        }
        if secondBtnTitle != ""
        {
            _ =  alertPopup.addButton("Cancel") {
                print("Cancel")
                
            }
            
        }
        
        if firstBtnTitle != "" || secondBtnTitle != "" || timeout == 1{
            alertPopup.showTitle("", subTitle: "", timeout: nil, completeText: "", style: style, colorStyle: 0x2D292B, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
        }
        else {
        let timeOut = SCLAlertView.SCLTimeoutConfiguration.init(timeoutValue: timeout, timeoutAction: {})
        alertPopup.showTitle(title, subTitle: message, timeout: timeOut, completeText: "", style: style, colorStyle: 0x2D292B, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            completed(true)
        }
        
    
    }
    
    func hide(){
        if alertPopup != nil {
            alertPopup.hideView()
        }
    }
}
