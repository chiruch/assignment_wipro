//
//  UITableViewExtension.swift
//  AssignmentApp
//
//  Created by Guest User on 4/4/19.
//  Copyright © 2019 Guest User. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func showNoDataUI(message:String) {
      self.backgroundView = createLabel(frame: self.frame, text: message, textColor: .black, textAlignMent: .center, numberOfLines: 0)
    }
}

func createLabel(frame:CGRect,text:String,textColor:UIColor,textAlignMent:NSTextAlignment,numberOfLines:Int)-> UILabel {
    let label = UILabel(frame: frame)
    label.text = text
    label.textColor = textColor
    label.textAlignment = textAlignMent
    label.numberOfLines = numberOfLines
    return label
}
