//
//  ListModel.swift
//  Assignment_WIPRO
//
//  Created by Chiranjeevi Challa on 31/05/19.
//  Copyright © 2019 Chiranjeevi Challa. All rights reserved.
//

import Foundation

struct AppViewData{
    
    let title: String
    let imageName: String
    let price:Double
    let currency:String
    let category:String
    let releaseData:String
    let summary:String
    
}
