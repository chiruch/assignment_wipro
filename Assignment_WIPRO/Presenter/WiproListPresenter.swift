//
//  WiproListPresenter.swift
//  Assignment_WIPRO
//
//  Created by Chiranjeevi Challa on 31/05/19.
//  Copyright © 2019 Chiranjeevi Challa. All rights reserved.
//

import Foundation

protocol AppListViewProtocol: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func updateUI()
    func showEmptyUI()
}

class ListPresenter {
    fileprivate let listService:APIService
    weak fileprivate var listViewProtocol:AppListViewProtocol?
    fileprivate var appsToDisplayArr = [AppViewData]()
    init(listService:APIService) {
     self.listService = listService
    }
    
    func confirmTo(_ listViewProtocol:AppListViewProtocol ) {
        self.listViewProtocol = listViewProtocol
    }
    
     func getAppsLisFromServer() {
        self.listViewProtocol?.startLoading()
        self.listService.fetchAppsData{ [weak self] appViewModel in
            self?.listViewProtocol?.finishLoading()
            self?.mapAppsToUI(with: appViewModel)
        }
    }
    
    func mapAppsToUI(with appViewModel:APPListModel) {
        if(appViewModel.feed?.entry?.count == 0){
            self.listViewProtocol?.showEmptyUI()
            self.listViewProtocol?.updateUI()
        }else{
            guard let mappedApps = (appViewModel.feed?.entry?.map { (entry) -> AppViewData in
                let title =  entry.imname!.label ?? ""
                var imageName = ""
                if let imimageArr = entry.imimage {
                    imageName = imimageArr[0].label ?? ""
                }
                let price = Double(entry.imprice?.attributes?.label ?? "") ?? 0.0
                let currency =  "SAR"
                let category = entry.category?.attributes?.label ?? ""
                let releaseDate = entry.imreleaseDate?.label ?? ""
                let summary = entry.summary?.label ?? ""
                return AppViewData(title:title, imageName:imageName , price: price, currency:currency, category: category, releaseData: releaseDate, summary: summary)
                }) else {
                    return
            }
            self.appsToDisplayArr = mappedApps
            self.listViewProtocol?.updateUI()
        }
        
    }
    
    func getNumberOfRows() -> Int {
        return appsToDisplayArr.count
    }
    
    func getAppData(at index:Int) -> AppViewData {
        return appsToDisplayArr[index]
    }
    
    
}
