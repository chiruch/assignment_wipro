//
//  APIService.swift
//  Assignment_WIPRO
//
//  Created by Chiranjeevi Challa on 31/05/19.
//  Copyright © 2019 Chiranjeevi Challa. All rights reserved.
//

import Foundation
import Alamofire
class APIService {
    
    var manager = Alamofire.SessionManager.default
    let header = [ "Content-Type": "application/x-www-form-urlencoded"]
    func fetchAppsData(completionHandler:@escaping (_ wipListModel:APPListModel)-> Void)  {
        
        manager.session.configuration.timeoutIntervalForRequest = 20
        
        manager.request(LIST_URL, method: HTTPMethod.get , encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            guard let data = response.data else { return }
            if let geAppModel = self.decodeData(data: data) {
                print(geAppModel.feed?.author ?? "")
                
                DispatchQueue.main.async {
                    
                    completionHandler(geAppModel)
                }
            }
        
    }
    
    
}
    
    func decodeData(data:Data) -> APPListModel? {
        do {
            let decoder = JSONDecoder()
            let geAppModel = try decoder.decode(APPListModel.self, from: data)
            return geAppModel
        } catch let err {
            print("Err", err)
            
            Message.shared.show("Error!!!", message:err.localizedDescription , firstBtnTitle: "", secondBtnTitle: "", style: .error, completed: { (isCompleted) in
            }
                
            )
            
        }
        return nil
    }
}

