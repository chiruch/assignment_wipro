//
//  ListView.swift
//  Assignment_WIPRO
//
//  Created by Chiranjeevi Challa on 31/05/19.
//  Copyright © 2019 Chiranjeevi Challa. All rights reserved.
//

import UIKit

class ListView: UITableViewController {
    fileprivate let appListPresenter = ListPresenter(listService:APIService())
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "Assignment"
        let reFresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reFreshTableView))
        navigationItem.rightBarButtonItem = reFresh
        
       tableView.register(ListViewCell.self, forCellReuseIdentifier: "Cell")
        appListPresenter.confirmTo(self)
        appListPresenter.getAppsLisFromServer()
        if appListPresenter.getNumberOfRows() > 0
        {
           self.title = appListPresenter.getAppData(at: 0).title
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return appListPresenter.getNumberOfRows()
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ListViewCell
        let appData = appListPresenter.getAppData(at: indexPath.row)
        cell.titleLabel.text = appData.title
        cell.setImageWith(urlString: appData.imageName)
        cell.descriptionLabel.text = appData.summary
        
            return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected Row:",indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func reFreshTableView()  {
        appListPresenter.getAppsLisFromServer()
    }
    
    
}

extension ListView:AppListViewProtocol {
    // To show Prograss view
    func startLoading() {
            Message.shared.show("Please Wait", message: "Downloading....." , firstBtnTitle: "", secondBtnTitle: "", style: .wait, completed: { (isCompleted) in
            }
        
)}
    // To hide Prograss view
    func finishLoading() {
      Message.shared.hide()
    }
    // To Update UI
    func updateUI() {
        tableView?.isHidden = false
        tableView?.reloadData()
    }
    //To Show empty UI
    func showEmptyUI() {
        // Show empty UI
         self.tableView?.showNoDataUI(message: "No Apps available to Display")
    }
}
