//
//  GEIm:image.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct GEImimage : Codable {

        let attributes : GEAttribute?
        let label : String?

        enum CodingKeys: String, CodingKey {
                case attributes = "attributes"
                case label = "label"
        }
}
