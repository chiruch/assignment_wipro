//
//  GEFeed.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct GEFeed : Codable {

        let author : GEAuthor?
        let entry : [GEEntry]?
        let icon : GEIcon?
        let id : GEId?
        let link : [GELink]?
        let rights : GERight?
        let title : GETitle?
        let updated : GEUpdated?

        enum CodingKeys: String, CodingKey {
                case author = "author"
                case entry = "entry"
                case icon = "icon"
                case id = "id"
                case link = "link"
                case rights = "rights"
                case title = "title"
                case updated = "updated"
        }
}
