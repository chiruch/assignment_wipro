//
//  GEEntry.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct GEEntry : Codable {

        let category : GECategory?
        let id : GEId?
        let imartist : GEImartist?
        let imcontentType : GEImcontentType?
        let imimage : [GEImimage]?
        let imname : GEImname?
        let imprice : GEImprice?
        let imreleaseDate : GEImreleaseDate?
        let link : GELink?
        let rights : GERight?
        let summary : GESummary?
        let title : GETitle?

        enum CodingKeys: String, CodingKey {
                case category = "category"
                case id = "id"
                case imartist = "im:artist"
                case imcontentType = "im:contentType"
                case imimage = "im:image"
                case imname = "im:name"
                case imprice = "im:price"
                case imreleaseDate = "im:releaseDate"
                case link = "link"
                case rights = "rights"
                case summary = "summary"
                case title = "title"
        }
}
