//
//  GELink.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct GELink : Codable {

        let attributes : GEAttribute?

        enum CodingKeys: String, CodingKey {
                case attributes = "attributes"
        }
}
