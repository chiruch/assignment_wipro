//
//  GEUri.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct GEUri : Codable {

        let label : String?

        enum CodingKeys: String, CodingKey {
                case label = "label"
        }
}
