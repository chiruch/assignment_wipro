//
//  GEAuthor.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct GEAuthor : Codable {

        let name : GEName?
        let uri : GEUri?

        enum CodingKeys: String, CodingKey {
                case name = "name"
                case uri = "uri"
        }
}
