//
//  GEAttribute.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct GEAttribute : Codable {

        let imid : String?
        let label : String?
        let scheme : String?
        let term : String?

        enum CodingKeys: String, CodingKey {
                case imid = "im:id"
                case label = "label"
                case scheme = "scheme"
                case term = "term"
        }
}
