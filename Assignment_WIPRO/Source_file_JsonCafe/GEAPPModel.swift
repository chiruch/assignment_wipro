//
//  APPListModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct APPListModel : Codable {

        let feed : GEFeed?

        enum CodingKeys: String, CodingKey {
                case feed = "feed"
        }
}
