//
//  GEIm:price.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 4, 2019

import Foundation

struct GEImprice : Codable {

        let attributes : GEAttribute?
        let label : String?

        enum CodingKeys: String, CodingKey {
                case attributes = "attributes"
                case label = "label"
        }


}

struct GEPriceAttribute:Codable {
    let amount : Double?
    let currency: String?
    
    enum CodingKeys: String, CodingKey {
        case amount = "amount"
        case currency = "currency"
    }
}
