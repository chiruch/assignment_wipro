//
//  Assignment_WIPROTests.swift
//  Assignment_WIPROTests
//
//  Created by Chiranjeevi Challa on 31/05/19.
//  Copyright © 2019 Chiranjeevi Challa. All rights reserved.
//

import XCTest
import Alamofire
@testable import Assignment_WIPRO

class Assignment_WIPROTests: XCTestCase {
    let listView = ListView()
    
    override func setUp() {
    }

    override func tearDown() {
    }
    
    //Unit test for title
    func testTitle(){
        let _ = listView.viewDidLoad()
       
        
        XCTAssertEqual(listView.title, "Assignment")
    }
    //Test ModelObject
    func testModelObject() {
        let appDataModel = AppViewData(title: "Hello", imageName: "SampleImage", price: 1.00, currency: "INR", category: "Games", releaseData: "10/10/2019", summary: "fwefewfgewfjebver")
        
        XCTAssertNotNil(appDataModel)
    }
    //Unit test for TableView
    func testHasATableView() {
        XCTAssertNotNil(listView.tableView)
    }
    
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(listView.tableView.delegate)
    }
    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(listView.conforms(to: UITableViewDelegate.self))
        XCTAssertTrue(listView.responds(to: #selector(listView.tableView(_:didSelectRowAt:))))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(listView.tableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(listView.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(listView.responds(to: #selector(listView.numberOfSections(in:))))
        XCTAssertTrue(listView.responds(to: #selector(listView.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(listView.responds(to: #selector(listView.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewCellHasReuseIdentifier() {
        let cell = listView.tableView(listView.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? ListViewCell
        let actualReuseIdentifer = cell?.reuseIdentifier
        let expectedReuseIdentifier = "Cell"
        XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
    }
    

    
   //Unit test for API call with Alamofire
    func testAlamofire() {
        let ex = expectation(description: "Alamofire")
        
        Alamofire.request(LIST_URL)
            .response { response in
                XCTAssertNil(response.error, "Failure, error \(response.error!.localizedDescription)")
                
                XCTAssertNotNil(response, "No response")
                XCTAssertEqual(response.response?.statusCode ?? 0, 200, "Status code not 200")
                
                ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    

    

}
